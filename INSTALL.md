# Installing
Installing is easy. Just run the following in your shell:
```bash
chmod +x execute prefix shoehorn
./prefix
```
# The "man" pages
Just ignore the seperator (:)
## execute
The `execute` script runs an app in the prefix.
It's syntax is:
```bash
./execute file
```
## prefix
The `prefix` script makes the prefix. This is mandatory, as the other scripts assume that it's there.
It accepts 0 arguments.
## shoehorn
The `shoehorn` script copies a file into the prefix. This is useful for doing such purposes like adding a library.
It's syntax is:
```bash
./shoehorn file : path for the container
```


